import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class Main {
    public static void main(String[] args) throws IOException {
        String fileName = "C:/Java projects/sber/TaskTwo/Задача ВС Java Сбер.csv";
        Path path = Paths.get(fileName);
        List<City> cityList = new ArrayList<>();

        Scanner scanner = new Scanner(path);

        scanner.useDelimiter(System.getProperty("line.separator"));
        while (scanner.hasNext()) {
            City city = parseCSVLine(scanner.next());
            cityList.add(city);
        }
        scanner.close();
        sortByCity(cityList);

        for (City city : cityList)
            System.out.println(city);


        sortByCityAndDistrict(cityList);

        for (City city : cityList)
            System.out.println(city);


    }

    private static City parseCSVLine(String line) {
        Scanner scanner = new Scanner(line);
        scanner.useDelimiter(";");
        scanner.next();
        String name = scanner.next();
        String region = scanner.next();
        String district = scanner.next();
        int population = scanner.nextInt();
        String foundation = null;
        if (scanner.hasNext()) {
            foundation = scanner.next();
        }

        return new City(name, region, district, population, foundation);
    }

    private static void sortByCity(List<City> cityList) {
        cityList.sort(Comparator.comparing(City::getName));
    }

    private static void sortByCityAndDistrict(List<City> cityList) {
        cityList.sort(Comparator.comparing(City::getDistrict).thenComparing(City::getName));
    }

}
